#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <random>
#include <chrono>
#include <sstream>
#include <string>

#include "analyze.hpp"
#include "simulator.hpp"
#include "transaction.hpp"

extern double lambda;
extern double nu;
extern double latency;
extern double bandwith;
extern double current_time;
extern int network_diameter;
extern double max_latency;
extern int inbox_size;
extern std::vector<Node *> g_nodes;
extern std::map<int, Transaction *> transactions_map;

using json = nlohmann::json;

template <typename T>
std::set<T> getUnion(const std::set<T> &a, const std::set<T> &b) {
        std::set<T> result = a;
        result.insert(b.begin(), b.end());
        return result;
}

Simulator::Simulator() {
        std::string line;
        std::ifstream myfile;
        myfile.open("graph.nx");

        getline(myfile, line);
        int N = stoi(line);

        for (int i = 0; i < N; i++) {
                Node *new_node = new Node(i, lambda, nu);
                g_nodes.push_back(new_node);
        }

        if (!myfile.is_open()) {
                perror("Error open");
                exit(EXIT_FAILURE);
        }
        while (getline(myfile, line)) {
                std::vector<std::string> edge_nodes;
                boost::split(edge_nodes, line, [](char c) {
                        return c == ';';
                });

                int first = stoi(edge_nodes[0]);
                int second = stoi(edge_nodes[1]);
                int edge_latency = stoi(edge_nodes[2]);

                g_nodes[first]->append_neighbor(g_nodes[second], latency);

                g_nodes[second]->append_neighbor(g_nodes[first], latency);
        }
}


void Simulator::run() {
        std::vector<Node*> tmp_nodes = g_nodes;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();


        std::shuffle(
                tmp_nodes.begin(),
                tmp_nodes.end(),
                std::default_random_engine(seed)
                );
        for (auto &node : tmp_nodes) {
                node->generate();
        }

        std::shuffle(
                tmp_nodes.begin(),
                tmp_nodes.end(),
                std::default_random_engine(seed)
                );
        for (auto &node : tmp_nodes) {
                node->empty_inbox();
        }

        analyzer.analyze_inbox();
}

void Simulator::end() {
        analyzer.write_inbox();
        //analyzer.analyze_parameters();
        Analyze::analyze_transactions_end();
}
