#include <fstream>
#include <iostream>

#include "analyze.hpp"
#include "node.hpp"
#include "transaction.hpp"

extern double gossip_rate;
extern double step;
extern int simulation_time;
extern int tx_size;
extern double current_time;
extern double lambda;
extern double nu;
extern double bandwith;
extern double pow_latency;
extern double latency;
extern int inbox_size;
extern int network_diameter;
extern double max_latency;
extern std::map<int, Transaction *> transactions_map;
extern std::vector<Node *> g_nodes;

Analyze::Analyze() {
        this->json_inbox = {};
}

void Analyze::analyze_inbox() {

        json json_nodes = {};

        for (auto &n : g_nodes) {
                json json_node;
                json_node["id"] = n->get_id();
                json_node["inbox"] = json::array();
                json_node["local_tangle"] = json::array();
                for(auto &elt : n->get_inbox()) {
                        json_node["inbox"].push_back(elt->get_id());
                }
                for(auto& elt: n->get_recently_added())
                {
                        json_node["local_tangle"].push_back(elt->get_id());
                }


                json_nodes.push_back(json_node);
        }

        this->json_inbox.push_back({{"time", current_time}, {"nodes", json_nodes}});
}

void Analyze::write_inbox() {
        std::ofstream o("output/inbox.json");
        o << std::setw(1) << this->json_inbox << std::endl;
}

void Analyze::analyze_transactions_end() {
        json output_json = {};
        for (auto &tx : transactions_map) {
                json tx_json;
                tx_json["id"] = tx.second->get_id();
                tx_json["timestamp"] = tx.second->get_timestamp();
                tx_json["confirmed"] = tx.second->is_confirmed();
                tx_json["confirmed_times"] = {};
                tx_json["fully_received"] = tx.second->is_fully_received();
                tx_json["gossiped_times"] = {};
                for (auto &n : g_nodes) {
                        tx_json["confirmed_times"].push_back(tx.second->get_time_added(n));
                        tx_json["received_times"].push_back(tx.second->get_time_received(n));
                }

                output_json.push_back(tx_json);
        }

        std::ofstream o("output/transactions.json");
        o << output_json << std::endl;
}

void Analyze::analyze_parameters()
{
        json json_param;
        json_param["max_latency"] = max_latency;
        json_param["number_nodes"] = g_nodes.size();
        json_param["gossip_rate"] = gossip_rate;
        json_param["step"] = step;
        json_param["lambda"] = lambda;
        json_param["nu"] = nu;
        json_param["latency"] = latency;
        json_param["inbox_size"] = inbox_size;
        json_param["network_diameter"] = network_diameter;
        json_param["simulation_time"] = simulation_time;
        std::ofstream o("output/parameters.json");
        o << std::setw(1) << json_param << std::endl;
}
