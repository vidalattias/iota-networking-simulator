#include <cmath>
#include <iostream>
#include <itpp/base/random.h>

#include "node.hpp"
#include "transaction.hpp"

extern double gossip_rate;
extern double step;
extern double current_time;
extern double pow_latency;
extern double nu;
extern int inbox_size;
extern int latency;
extern double max_latency;
extern std::map<int, Transaction*> transactions_map;

Node::Node(int _id, double _lambda, double _nu)
{
        id = _id;
        lambda = _lambda;
        nu = _nu;
        next_generation_time = current_time;
        next_processing_time = current_time;
        next_transmission_time = current_time;
        generated = 0;
}

std::set<int> Node::get_tangle()
{
        return local_tangle;
}

std::vector<Transaction*> Node::get_recently_added()
{
        return recently_added;
}

std::set<int> Node::get_tangle_past()
{
        std::set<int> return_tangle;

        for(auto& tx : local_tangle)
        {
                if(transactions_map[tx]->get_timestamp() < current_time - 3*max_latency)
                {
                        return_tangle.insert(tx);
                }
        }
        return return_tangle;
        return local_tangle;
}

void Node::receive_tx(Transaction* _tx, double _arrival_time)
{
        if(this->received_tx[_tx->get_id()] == false)
        {
                this->received_tx[_tx->get_id()] = true;
                _tx->received(this, current_time);
                this->waiting_list.push_back(std::pair<Transaction*, double>(_tx, _arrival_time));
        }
}

std::vector<Transaction*> Node::get_inbox()
{
        return inbox;
}

void Node::append_neighbor(Node* _node, double _latency)
{
        if(_node->get_id() != this->id)
        {
                neighbors.insert(std::pair<Node*, double>(_node, _latency));
        }
}

int Node::get_id()
{
        return id;
}

void Node::gossip(Transaction* _tx)
{
        int count_sent = 0;
        for(auto& neighbor : neighbors)
        {
                if(neighbor.first != _tx->get_issuer() && neighbor.first != this)
                {
                        if(1)
                        {
                                neighbor.first->receive_tx(_tx, neighbor.second);
                                count_sent++;
                        }
                }
        }
}

void Node::generate()
{
        //itpp::Exponential_RNG lambda_rate(lambda);
        while(next_generation_time < current_time + step)
        {
                //next_generation_time += lambda_rate() + pow_latency;
                next_generation_time += 1/lambda;

                if(next_generation_time <= current_time + step)
                {
                        generated++;
                        Transaction* new_tx = new Transaction(
                                Transaction::new_tx_id(),
                                current_time,
                                rand() % 100,
                                this
                                );


                        transactions_map.insert(std::pair(new_tx->get_id(), new_tx));
                        this->receive_tx(new_tx, current_time);
                        //this->receive_tx(new_tx, latency);
                        //this->gossip(new_tx);
                        //std::cerr << "Node nº" << this->id << " generated tx nº" << new_tx->get_id() << std::endl;
                }
        }
}


void print_inbox(std::vector<Transaction*> in, int id)
{
        std::clog << "Reading inbox of node nº" << id << " of size " << in.size() << std::endl;
        for(auto elt : in)
        {
                std::clog << "\t" << elt->get_id() << std::endl;
                std::clog << "\t\ttimestamp : " << elt->get_timestamp() << std::endl;
                std::clog << "\t\tgap : " << elt->get_gap() << std::endl;
        }
}

void print_inbox(std::set<Transaction*> in, int id)
{
        std::clog << "Reading inbox of node nº" << id << " of size " << in.size() << std::endl;
        for(auto elt : in)
        {
                std::clog << "\t" << elt->get_id() << std::endl;
                std::clog << "\t\ttimestamp : " << elt->get_timestamp() << std::endl;
                std::clog << "\t\tgap : " << elt->get_gap() << std::endl;
        }
}

void Node::empty_inbox()
{
        std::vector<std::pair<Transaction*, double> >::iterator it_wait = waiting_list.begin();

        while(it_wait != waiting_list.end())
        {
                if(current_time + step >= it_wait->second)
                {
                        inbox.push_back((*it_wait).first);
                        it_wait = waiting_list.erase(it_wait);
                }
                else
                        it_wait++;
        }

        double lower_bound = current_time - 2*max_latency;
        double upper_bound = current_time - max_latency;

        double window = upper_bound-lower_bound;

        std::vector<Transaction*> to_gossip;

        auto it = inbox.begin();
        while(it != inbox.end())
        {
                if((*it)->get_timestamp() < lower_bound)
                {
                        it = inbox.erase(it);
                }
                else if((*it)->get_timestamp() < current_time-window)
                {
                        to_gossip.push_back(*it);
                        it++;
                }
                else
                {
                        it++;
                }
        }



        sort(to_gossip.begin(), to_gossip.end(), Transaction::compare_transaction);

        if(to_gossip.size() > inbox_size)
        {
                to_gossip = std::vector(to_gossip.begin(), to_gossip.begin() + inbox_size);
        }





        //itpp::Exponential_RNG nu_rate(nu);

        auto it_gossip = to_gossip.begin();



        while(next_processing_time <= current_time + step && it_gossip != to_gossip.end())
        {
                this->gossip(*it_gossip);

                to_add_to_tangle.push_back(*it_gossip);

                auto it_find = find(inbox.begin(), inbox.end(), *it_gossip);
                inbox.erase(it_find);

                it_gossip = to_gossip.erase(it_gossip);


                if(it_gossip != to_gossip.end())
                        //next_processing_time += nu_rate();
                        next_processing_time += 1/nu;
        }


        auto add_it = to_add_to_tangle.begin();
        //recently_added is used only for logs
        recently_added.clear();
        while(add_it != to_add_to_tangle.end())
        {
                if((*add_it)->get_timestamp() <= upper_bound)
                {
                        local_tangle.insert((*add_it)->get_id());
                        recently_added.push_back(*add_it);
                        (*add_it)->added_to_tangle(this, current_time);
                        add_it = to_add_to_tangle.erase(add_it);
                }
                else
                {
                        add_it++;
                }
        }
}
