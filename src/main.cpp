#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <itpp/base/random.h>
#include <map>
#include <string>
#include <nlohmann/json.hpp>


#include "progress_bar.hpp"
#include "simulator.hpp"

class Node;
class Transaction;

double gossip_rate;
double step;
int simulation_time;
double current_time;
double lambda;
double nu;
double latency;
int inbox_size;
int network_diameter;
double max_latency;
std::map<int, Transaction *> transactions_map;
std::vector<Node *> g_nodes;

using json = nlohmann::json;


int main() {
        std::ifstream file_raw("output/parameters.json");
        json json_file;
        file_raw >> json_file;

        gossip_rate = json_file["gossip_rate"];
        step = json_file["step"];
        simulation_time = json_file["simulation_time"];
        lambda = json_file["lambda"];
        nu = json_file["nu"];
        latency = json_file["latency"];
        inbox_size = json_file["inbox_size"];
        network_diameter = json_file["network_diameter"];
        max_latency = json_file["max_latency"];


        srand(time(NULL));
        itpp::GlobalRNG_randomize();

        Simulator sim = Simulator();

        ProgressBar progress_bar(simulation_time / step);

        for (int i = 0; i < simulation_time / step; i++) {
                progress_bar.Progressed(i);
                current_time += step;
                sim.run();
        }

        sim.end();
        return 0;
}
