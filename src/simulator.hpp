#ifndef SIMULATOR_HPP
#define SIMULATOR_HPP

#include "node.hpp"
#include "analyze.hpp"

class Simulator
{
private:
///Represents the set of Node we are using in the network.
Analyze analyzer;
public:
///Constructor of the simulator. It fetches in the file graph.nx the network topology and builds the nodes and their neighboring relations.
Simulator();

///Runs a step of the simulation by calling node by node the following routines in order : Node::generate(), Node::empty_inbox() and Node::empty_outbox() and then calls Simulator::analyze().
void run();

///Runs an analysis of the network at the current time by returning the ration between the intersection of all the local tangles and the union of all the local tangles. A value of 1 represents a full consensus on the network and 0 means that the nodes does not agree. Note that a possible drawback of this metric is that if one node is disconnected and sees no transaction, it will return 0 although all the other nodes may have the same view of the tangle.
void analyze();

void end();
};

#endif
