#include <iostream>
#include <vector>

#include "transaction.hpp"

extern double current_time;
extern std::vector<Node*> g_nodes;

Transaction::Transaction(int _id, double _timestamp, int _gap, Node* _issuer)
{
        this->id = _id;
        this->timestamp = _timestamp;
        this->gap = _gap;
        this->issuer = _issuer;
        this->count_added = 0;
        this->count_received = 0;
        this->confirmed = false;
        this->fully_received = false;

        for(auto& elt : g_nodes)
        {
                time_added[elt] = -1;
                time_received[elt] = -1;
        }
}

int Transaction::get_gap()
{
        return this->gap;
}

Node* Transaction::get_issuer()
{
        return this->issuer;
}

int Transaction::new_tx_id()
{
        static int tx_count = 0;
        return tx_count++;
}

int Transaction::get_id()
{
        return id;
}

double Transaction::get_timestamp()
{
        return this->timestamp;
}

bool Transaction::compare_transaction(Transaction* _tx1, Transaction* _tx2)
{
        return (_tx1->get_gap() > _tx2->get_gap());
}

void Transaction::added_to_tangle(Node* _node, double _time)
{
        this->time_added[_node] = _time;
        this->count_added++;

        if(count_added == g_nodes.size())
        {
                confirmed = true;
        }
}

double Transaction::get_time_added(Node* _node)
{
        return time_added[_node];
}

bool Transaction::is_confirmed()
{
        return this->confirmed;
}


void Transaction::received(Node* _node, double _time)
{
        if(this->time_received[_node] == -1)
        {
                this->count_received++;
                this->time_received[_node] = _time;
        }

        if(count_received == g_nodes.size())
        {
                fully_received = true;
        }
}

double Transaction::get_time_received(Node* _node)
{
        return time_received[_node];
}

bool Transaction::is_fully_received()
{
        return this->fully_received;
}
