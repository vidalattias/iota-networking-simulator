#ifndef NODE_HPP
#define NODE_HPP

#include <vector>
#include <map>
#include <set>

class Transaction;

class Node
{
private:
/// Node's unique identifier. Should be a number between 0 and the amount of nodes N.
int id;
/// Node's Transaction generation rate.
double lambda;
/// Node's inbox processing rate.
double nu;

double next_generation_time;
double next_processing_time;
double next_transmission_time;
///Vector containing the transactions sent to the Node and not seen yet because of network latency.
std::vector<std::pair<Transaction*, double> > waiting_list;
/// The set of transactions that the node has received and must process to append to it's local tangle and then gossip. It is proccessed at speed nu. Each element is a pair of a Transaction and its arrival time, i.e, its transmission time plus the latency to this node.
std::vector<Transaction*> inbox;

std::vector<Transaction*> to_add_to_tangle;
std::vector<Transaction*> recently_added;

///This is the map of the neighbors of this node. For each other neighboring node is associated the communication latency toward this node.
std::map<Node*, double> neighbors;

/// The local view of the Tangle for this node. It is considered as a set for performance considerations.
std::set<int> local_tangle;
std::map<int, bool> received_tx;

public:
///Node's constructor. It simply associates the arguments values to the attributes.
Node(int _id, double _lambda, double _nu);

///Returns the node's identifier.
int get_id();
std::vector<Transaction*> get_recently_added();
///Returns the node's local Tangle.
std::set<int> get_tangle();
std::set<int> get_tangle_past();

///Appends a neighbor to the neighbors list with associated network latency.
void append_neighbor(Node* _node, double _latency);

///Method used by another node to send a transaction. The _arrival_time parameter is the time of sending + the network latency. Upon reception, the node will put the transaction in the waiting_list.
void receive_tx(Transaction* _tx, double _arrival_time);

///Returns the inbox upon query.
std::vector<Transaction*> get_inbox();

///The inbox emptying routine. It begins by iterating through the waiting_list attribute to find transactions that have arrived since the last simulation time due to latency and place them in the inbox. Each time a transaction is removed from the waiting_list, we sort the inbox according to the transaction's gap value.
void empty_inbox();

///Generates the node's transactions at rate lambda. It increases the next_generation_time by a random value following a Poisson law of variable lambda. When a transaction is generated, it appends it to its local tangle and to its outbox, waiting to be gossiped during the Node::empty_outbox() routine.
void generate();


int generated;
//This routine gossips a Transaction. For each neighbor of the node, it will pick a random value in [0,1]. If the value is under the gossip_rate global variable, it will call the neighbor's member function Node::receive_tx() with parameters _tx and current_time+latency.
void gossip(Transaction* _tx);
};

#endif
