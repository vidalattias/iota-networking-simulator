#ifndef ANALYSE_HPP
#define ANALYSE_HPP

#include <nlohmann/json.hpp>
using json = nlohmann::json;

class Analyze {
private:
json json_inbox;

public:
Analyze();
void analyze_inbox();
void write_inbox();
static void analyze_transactions_end();
static void analyze_parameters();
};
#endif
