import matplotlib.pyplot as plt
import json
import math
import numpy as np

with open("output/parameters.json") as f:
    params = json.load(f)

with open("output/transactions.json") as f:
    txs = json.load(f)


max_received = []
max_received_times = []
for tx in txs:
    receive_relative = [elt - tx["timestamp"] for elt in tx["received_times"]]
    max_received.append(max(receive_relative))
    max_received_times.append(tx["timestamp"])

plt.plot(max_received_times, max_received, 'ro')
plt.show()
