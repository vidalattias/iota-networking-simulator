import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import json

with open("input.json") as f:
    param = json.load(f)

k = param["number_neighbors"]
N = param["number_nodes"]

G = nx.random_regular_graph(k, N)
nx.draw_networkx(G)
plt.savefig('graph.pdf')
# plt.show()
# Get adjacency matrix and weight by delay at each channel
# ChannelDelays = 0.9 * np.ones((N, N)) + 0.2 * np.random.rand(N, N)
# AdjMatrix = np.multiply(1 * np.asarray(nx.to_numpy_matrix(G)), ChannelDelays)

new_file = open("graph.nx", "w")
new_file.write(str(N) + "\n")

for edge in G.edges():
    new_file.write(str(edge[0]) + ";" + str(edge[1]) + ";0\n")

new_file.close()

diameter = nx.diameter(G)
max_latency = 2 * (diameter + 2) * \
    (param["latency"] + param["inbox_size"] / param["nu"])

param["network_diameter"] = diameter
param["max_latency"] = max_latency

with open("output/parameters.json", "w") as f:
    f.write(json.dumps(param, indent=4))
