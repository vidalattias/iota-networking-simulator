import matplotlib.pyplot as plt
import json
import numpy as np


with open("output/inbox.json") as f:
    data = json.load(f)
with open("output/parameters.json") as f:
    param = json.load(f)
with open("output/transactions.json") as f:
    txs = json.load(f)

N = param["number_nodes"]
times = [x["time"] for x in data]


for i in range(param["number_nodes"]):
    inbox = []
    for x in data:
        inbox.append(len(x["nodes"][i]["inbox"]))

    plt.plot(times, inbox)

plt.title("Evolution of inboxes for each node over time")
plt.xlabel("Time (abstract unit)")
plt.ylabel("Inbox size")
plt.show()


for i in range(param["number_nodes"]):
    local_tangle = []
    for x in data:
        local_tangle.append(len(x["nodes"][i]["local_tangle"]))

    plt.plot(times, local_tangle)

plt.title("Evolution of local tangles for each node over time")
plt.xlabel("Time (abstract unit)")
plt.ylabel("Inbox size")
plt.show()


union_list = [[] for i in range(N)]
inter_list = [[] for i in range(N)]

for n in range(N):
    union_list[n].append(set(data[0]["nodes"][n]["local_tangle"]))

for i, x in enumerate(data[1:]):
    for n in range(N):
        union_list[n].append(union_list[n][i].union(
            x["nodes"][n]["local_tangle"]))

for n in range(N):
    plot_union = []
    for x in union_list[n]:
        plot_union.append(len(x))
    plt.plot(plot_union)
plt.title("Size of union list")
plt.show()


rate_list = []
for i, _ in enumerate(times):
    union_tmp = set()
    for n in range(N):
        union_tmp = union_tmp.union(union_list[n][i])
    inter_tmp = set(union_tmp)

    for n in range(N):
        inter_tmp = inter_tmp.union(union_list[n][i])

    if(len(union_tmp) > 0):
        rate_list.append(100 * len(inter_tmp) / len(union_tmp))
    else:
        rate_list.append(0)

plt.plot(times, rate_list)
plt.title("Intersection percentage of all local tangles over time")
plt.xlabel("Time (abstract unit)")
plt.ylabel("Percentage")
plt.ylim([-5, 105])
plt.show()
