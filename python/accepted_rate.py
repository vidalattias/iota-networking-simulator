import numpy as np
import json
import matplotlib.pyplot as plt

with open("output/transactions.json") as f:
    txs = json.load(f)
with open("output/parameters.json") as f:
    param = json.load(f)

new_txs = []
for tx in txs:
    new_txs.append([tx["timestamp"], tx["confirmed"]])

new_txs = sorted(new_txs)

i = 0
N = len(new_txs)
time = 0
times = []

accepted = []
rejected = []
accepted_rate = []

while i < N:
    times.append(time)
    temp_txs = []
    while(i < N and new_txs[i][0] <= time + param["max_latency"]):
        temp_txs.append(new_txs[i][1])
        i += 1

    accepted_rate.append(100 * temp_txs.count(True) / len(temp_txs))
    accepted.append(temp_txs.count(True))
    rejected.append(temp_txs.count(False))
    time += param["max_latency"]

accepted_rate.append(accepted_rate[-1])
accepted.append(accepted[-1])
rejected.append(rejected[-1])
sum = [accepted[i] + rejected[i] for i in range(len(accepted))]

times.append(time)
#plt.plot(times, accepted_rate)
plt.plot(times, accepted, label="Accepted")
plt.plot(times, rejected, label="Rejected")
plt.plot(times, sum, label="Sum")
plt.legend()
plt.show()
