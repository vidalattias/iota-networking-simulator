import numpy as np
import json

with open("output/transactions.json") as f:
    txs = json.load(f)
with open("output/parameters.json") as f:
    param = json.load(f)
with open("output/inbox.json") as f:
    inboxes = json.load(f)


new_txs = []
for i, tx in enumerate(txs):
    new_txs.append([tx["timestamp"], i])

N = len(txs)
new_txs = sorted(new_txs)
slotted_txs = []

time = 0
i = 0

# Here we compute slotted_txs
while i < N:
    tmp_slotted = []
    while i < N and txs[new_txs[i][1]]["timestamp"] < time + param["max_latency"]:
        tmp_slotted.append(txs[new_txs[i][1]])
        i += 1
    time += param["max_latency"]
    slotted_txs.append(tmp_slotted)


# Here we compute the average amount of txs per slot
slots_len = [len(x) for x in slotted_txs]
average_slots_len = np.mean(slots_len)

# Here we compute the average added to tangle per slot
slots_added_count = []
slots_added_percentage = []

for x in slotted_txs:
    count = 0
    for tx in x:
        if tx["confirmed"] == True:
            count += 1
    slots_added_count.append(count)
    slots_added_percentage.append(100 * count / len(x))


time = 0
i = 0
M = len(inboxes)
NN = param["number_nodes"]
slotted_inboxes = []
slotted_inboxes_size = []
# Here we compute slotted_txs
while i < M:
    union_sets = [set() for j in range(NN)]
    k = i
    while i < M and inboxes[i]["time"] < time + param["max_latency"]:
        for j in range(NN):
            union_sets[j] = union_sets[j].union(
                set(inboxes[i]["nodes"][j]["local_tangle"]))
        i += 1

    inter_set = [set(union_sets[j]) for j in range(NN)]

    i = k
    while i < M and inboxes[i]["time"] < time + param["max_latency"]:
        for j in range(NN):
            inter_set[j] = inter_set[j].intersection(
                set(inboxes[i]["nodes"][j]["local_tangle"]))
        i += 1

    time += param["max_latency"]

    if len(union_sets) > 0:
        slotted_inboxes.append(100 * len(inter_set) / len(union_sets))
    else:
        slotted_inboxes.append(0)
    slotted_inboxes_size.append(len(union_sets))


json_return = {}

json_return["generated_txs"] = len(txs)
print("Amount of generated transactions :")
print("\t" + str(json_return["generated_txs"]))

json_return["window_size"] = param["max_latency"]
print("Size of window :")
print("\t" + str(json_return["window_size"]))

json_return["avg_tx_per_window"] = average_slots_len
print("Average transaction per window :")
print("\t" + str(int(json_return["avg_tx_per_window"])))

json_return["avg_validated_per_window"] = np.mean(slots_added_count)
print("Average amount of validated transaction per window :")
print("\t" + str(int(json_return["avg_validated_per_window"])))

json_return["avg_percent_validated_per_window"] = np.mean(
    slots_added_percentage)
print("Average percentage of validated transaction per window :")
print("\t" + str(int(json_return["avg_percent_validated_per_window"])) + "%")

print("Average coherence between the local tangles per window :")
print("\t" + str(int(np.mean(slotted_inboxes))) + "%")

with open("output/analyzis_" + str(param["nu"]) + ".json", "w") as f:
    f.write(json.dumps(json_return, indent=4))
