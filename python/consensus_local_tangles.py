import numpy as np
import json
import matplotlib.pyplot as plt

with open("output/transactions.json") as f:
    txs = json.load(f)
with open("output/parameters.json") as f:
    param = json.load(f)
with open("output/inbox.json") as f:
    inboxes = json.load(f)

sort_inbox = []

for (i, x) in enumerate(inboxes):
    sort_inbox.append([x["time"], i])
sort_inbox = sorted(sort_inbox)
N = len(sort_inbox)


time = 0
times = []
j = 0
index = sort_inbox[j][1]

while j < N:
    times.append(time)
    while j < N and inboxes[index]["time"] < time + param["max_latency"]:
