# IOTA-networking-simulator

## Introduction

This document is a report of description of the simulator built for research purpose in order to build a robust networking layer for the IOTA protocol.

## Global structure

We try to make a continuous time simulation by using a simulation time step which can be as low as wished. For each step of time, we iterate through the nodes and make the following operations

1. Node::generate()}: The node will check if time has come to generate a transaction, according to its rate λ and the PoW latency h_PoW. If so, it will create a new object Transaction, append it to its local Tangle and append it to its outbox.

2. Node::empty_inbox() : The node processes the transactions in the inbox at the processing rate ν, appends the transaction to its local Tangle and to its outbox.

3. Node::empty_outbox() : The node processes the transactions in the outbox at rate τ by gossiping them to their neighbors and erasing them from the outbox.

## Detailed behaviour

### Network topology

So far, the network topology is an undirected k-regular graph or N nodes, each of them having k neighbors.

### Network latency

The network latency is represented as a weight associated to the network graph's edges. For performance issues, we have associated to each node a waiting_list in which each transaction is placed when gossiped and it stays there during the whole latency delay and then is placed in hte inbox. This waiting_list emptying is done at the very beginning of the Node::empty_inbox() phase, before actually emptying the inbox.

### Gossiping

We have implemented a rudimentary gossiping protocol.
